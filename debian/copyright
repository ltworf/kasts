Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Kasts
Upstream-Contact: Bart De Vries <bart@mogwai.be>
Source: https://invent.kde.org/plasma-mobile/kasts

Files: *
Copyright: 2021-2023, Bart De Vries <bart@mogwai.be>
           2021, Devin Lin <devin@kde.org>
           2020, Devin Lin <espidev@gmail.com>
           2021, Swapnil Tripathi <swapnil06.st@gmail.com>
           2020-2021, Tobias Fella <tobias.fella@kde.org>
License: GPL-2+3+KDEeV

Files: po/*
Copyright: 2021-2023, Steve Allewell <steve.allewell@gmail.com>
           2021-2023, Shinjo Park <kde@peremen.name>
           2021-2023, Eloy Cuadra <ecuadra@eloihr.net>
           2021-2023, Tommi Nieminen <translator@legisign.org>
           2021-2023, Matjaž Jeran <matjaz.jeran@amis.net>
           2021, Luiz Fernando Ranghetti <elchevive@opensuse.org>
           2021-2023, Antoni Bella Pérez <antonibella5@yahoo.com>
           2021-2023, Josep M. Ferrer <txemaq@gmail.com>
           2021, A S Alam <aalam@satluj.org>
           2021-2023, Yuri Chornoivan <yurchor@ukr.net>
           2021-2023, Vincenzo Reale <smart2128vr@gmail.com>
           2021-2023, Vit Pelcak <vit@pelcak.org>
           2021-2023, Xavier Besnard <xavier.besnard@neuf.fr>
           2021-2023, Stefan Asserhäll <stefan.asserhall@bredband.net>
           2021, Roman Paholik <wizzardsk@gmail.com>
           2021-2023, Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
           2021, Ignacy Kajdan <ignacy.kajdan@gmail.com>
           2021-2023, Freek de Kruijf <freekdekruijf@kde.nl>
           2023, Iñigo Salvador Azurmendi <xalba@ni.eus>
           2022-2023, Emir SARI <emir_sari@icloud.com>
           2021-2023, Antoni Bella Pérez <antonibella5@yahoo.com>
           2021-2023, Josep M. Ferrer <txemaq@gmail.com>
           2021, Burkhard Lück <lueck@hube-lueck.de>
           2022-2023, Frederik Schwarzer <schwarzer@kde.org>
           2022, Frank Steinmetzger <dev-kde@felsenfleischer.de>
           2023, José Nuno Coelho Pires <zepires@gmail.com>
License: GPL-2+3+KDEeV

Files: *.yml
       android/AndroidManifest.xml
       CMakeLists.txt
       .patches/run_kasts.sh
       src/CMakeLists.txt
       src/kmediasession/CMakeLists.txt
       src/kmediasession/example-app/CMakeLists.txt
       src/kmediasession/qml/CMakeLists.txt
       src/kmediasession/qml/qmldir.license
Copyright: 2022-2023, Bart De Vries <bart@mogwai.be>
           2020, Nicolas Fella <nicolas.fella@gmx.de>
           2020-2021, Tobias Fella <tobias.fella@kde.org>
           2020, Volker Krause <vkrause@kde.org>
License: BSD-2-Clause

Files: android/build.gradle
       android/version.gradle.in
       src/kmediasession/cmake/FindLIBVLC.cmake
       src/solidextras/android/build.gradle
       src/solidextras/android/CMakeLists.txt
       src/solidextras/CMakeLists.txt
       src/solidextras/qml/CMakeLists.txt
Copyright: 2023, <c> Bart De Vries <bart@mogwai.be>
           2011, <c> Harald Sitter <sitter@kde.org>
           2010, <c> Rohit Yadav <rohityadav89@gmail.com>
           2023, Bart De Vries <bart@mogwai.be>
           2020, Gabriel Souza Franco <gabrielfrancosouza@gmail.com>
           2019, Nicolas Fella <nicolas.fella@gmx.de>
           2018-2021, Volker Krause <vkrause@kde.org>
License: BSD-3-Clause

Files: .editorconfig
       .gitignore
       org.kde.kasts.appdata.xml
       org.kde.kasts.desktop
       src/kmediasession/dbus-interfaces/org.freedesktop.PowerManagement.Inhibit.xml
       src/kmediasession/dbus-interfaces/org.gnome.SessionManager.xml
       src/Messages.sh
       src/solidextras/android/AndroidManifest.xml
       src/solidextras/android/KastsSolidExtras-android-dependencies.xml
       src/solidextras/qml/qmldir.license
Copyright: 2021-2022, Bart De Vries <bart@mogwai.be>
           2020, Tobias Fella <tobias.fella@kde.org>
           2020, Volker Krause <vkrause@kde.org>
License: CC0-1.0

Files: src/kmediasession/example-app/resources.qrc
       src/resources-flatpak.qrc
       src/resources-non-flatpak.qrc
       src/resources.qrc
Copyright: 2021-2023, Bart De Vries <bart@mogwai.be>
           2020, Tobias Fella <tobias.fella@kde.org>
License: CC0-1.0

Files: src/qml/Desktop/HeaderBar.qml
       src/qml/Mobile/BottomToolbar.qml
Copyright: 2021-2023, Bart De Vries <bart@mogwai.be>
           2021, Swapnil Tripathi <swapnil06.st@gmail.com>
License: GPL-2.0-or-later

Files: src/qml/FullScreenImage.qml
Copyright: 2023, Bart De Vries <bart@mogwai.be>
License: GPL-3.0-only

Files: src/kmediasession/mpris2/mediaplayer2.*
       src/kmediasession/mpris2/mediaplayer2player.*
       src/kmediasession/mpris2/mpris2.*
Copyright: 2014, Ashish Madeti <ashishmadeti@gmail.com>
           2022-2023, Bart De Vries <bart@mogwai.be>
           2016, Matthieu Gallien <matthieu_gallien@yahoo.fr>
           2014, Sujith Haridasan <sujith.haridasan@kdemail.net>
License: GPL-3.0-or-later

Files: src/solidextras/android/org/kde/solidextras/NetworkStatus.java
       src/solidextras/config-solid-extras.h.in
       src/solidextras/networkstatus_android.cpp
       src/solidextras/networkstatus.cpp
       src/solidextras/networkstatus_dbus.cpp
       src/solidextras/networkstatus.h
       src/solidextras/org.freedesktop.portal.NetworkMonitor.xml
       src/solidextras/qml/solidextrasqmlplugin.cpp
Copyright: 2016, Red Hat Inc.
           2020, Volker Krause <vkrause@kde.org>
License: LGPL-2.0-or-later

Files: src/kmediasession/config-kmediasession.h.in
       src/kmediasession/example-app/main.cpp
       src/kmediasession/example-app/qml/main.qml
       src/kmediasession/kmediasession.*
       src/kmediasession/mediabackends/abstractmediabackend.h
       src/kmediasession/mediabackends/gstmediabackend.*
       src/kmediasession/mediabackends/qtmediabackend.*
       src/kmediasession/metadata.*
       src/kmediasession/qml/kmediasession-qmlplugin.cpp
Copyright: 2022-2023, Bart De Vries <bart@mogwai.be>
License: LGPL-2.1+3+KDEeV

Files: src/kmediasession/dbus-interfaces/org.mpris.MediaPlayer2.Player.xml
       src/kmediasession/dbus-interfaces/org.mpris.MediaPlayer2.xml
       src/kmediasession/.patches/vlc-ignore-time-for-cache.patch
Copyright: 2019, Mathieu Velten <matmaul@gmail.com>
License: LGPL-2.1-or-later

Files: src/audiomanager.*
       src/kmediasession/mediabackends/vlcmediabackend.*
       src/kmediasession/powermanagement/powermanagementinterface.*
       src/qml/UpdateNotification.qml
Copyright: 2017, <c> Matthieu Gallien <matthieu_gallien@yahoo.fr>
           2021-2023, Bart De Vries <bart@mogwai.be>
           2017-2019, Matthieu Gallien <matthieu_gallien@yahoo.fr>
License: LGPL-3.0-or-later

Files: android/res/drawable/splash.xml
Copyright: 2020 Tobias Fella <fella@posteo.de>
License: BSD-2-Clause

Files: src/settingsmanager.kcfgc src/settingsmanager.kcfg
Copyright: 2020-2023 Bart De Vries <bart@mogwai.be>
License: CC0-1.0

Files: kasts.svg kasts-android-square.svg logo.png android/ic_launcher-playstore.png android/res/drawable/kasts.png android/res/drawable-v24/ic_launcher_background.xml android/res/drawable-v24/ic_launcher_foreground.xml android/res/mipmap-hdpi/ic_launcher.png android/res/mipmap-mdpi/ic_launcher.png android/res/mipmap-xhdpi/ic_launcher.png android/res/mipmap-xxhdpi/ic_launcher.png android/res/mipmap-xxxhdpi/ic_launcher.png
Copyright: 2021 Mathis Brüchert <mbblp@protonmail.ch>
License: CC-BY-SA-4.0

Files: android/res/mipmap-anydpi-v26/ic_launcher.xml
Copyright: Souza Franco
License: CC0-1.0

Files: debian/*
Copyright: 2023 Salvo "LtWorf" Tomaselli <tiposchi@tiscali.it>
License: GPL-2+3+KDEeV

License: GPL-2.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-3.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: LGPL-2.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2.1-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License as published by
 the Free Software Foundation; either version 2.1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-3.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License as published by
 the Free Software Foundation; either version 3, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in `/usr/share/common-licenses/LGPL-3'.

License: GPL-3.0-only
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 3 as
 published by  the Free Software Foundation
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: GPL-2+3+KDEeV
 GPL-2
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; version 2.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 USA.
 .
 On Debian systems, GNU General Public License
 as published by the Free Software Foundation; version 2
 can be found in "/usr/share/common-licenses/GPL-2".
 .
 GPL-3
 This program is free software: you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation, either version
 3 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.
 .
 KDEeV
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of
 the License or (at your option) version 3 or any later version
 accepted by the membership of KDE e.V. (or its successor appro-
 ved by the membership of KDE e.V.), which shall act as a proxy
 defined in Section 14 of version 3 of the license.

License: LGPL-2.1+3+KDEeV
 LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation;
 version 2.1.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Library General Public
 License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".
 .
 LGPL-3
 This package is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-3'.
 .
 KDEeV
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.


License: CC0-1.0
 On Debian systems, the complete text can be found in /usr/share/common-licenses/CC0-1.0

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: CC-BY-SA-4.0
 Creative Commons Attribution-ShareAlike 4.0 International Creative Commons
 Corporation ("Creative Commons") is not a law firm and does not provide legal
 services or legal advice. Distribution of Creative Commons public licenses
 does not create a lawyer-client or other relationship. Creative Commons makes
 its licenses and related information available on an "as-is" basis. Creative
 Commons gives no warranties regarding its licenses, any material licensed
 under their terms and conditions, or any related information. Creative Commons
 disclaims all liability for damages resulting from their use to the fullest
 extent possible.
 .
 Using Creative Commons Public Licenses
 .
 Creative Commons public licenses provide a standard set of terms and conditions
 that creators and other rights holders may use to share original works of
 authorship and other material subject to copyright and certain other rights
 specified in the public license below. The following considerations are for
 informational purposes only, are not exhaustive, and do not form part of our
 licenses.
 .
 Considerations for licensors: Our public licenses are intended for use by
 those authorized to give the public permission to use material in ways otherwise
 restricted by copyright and certain other rights. Our licenses are irrevocable.
 Licensors should read and understand the terms and conditions of the license
 they choose before applying it. Licensors should also secure all rights necessary
 before applying our licenses so that the public can reuse the material as
 expected. Licensors should clearly mark any material not subject to the license.
 This includes other CC-licensed material, or material used under an exception
 or limitation to copyright. More considerations for licensors : wiki.creativecommons.org/Considerations_for_licensors
 .
 Considerations for the public: By using one of our public licenses, a licensor
 grants the public permission to use the licensed material under specified
 terms and conditions. If the licensor's permission is not necessary for any
 reason–for example, because of any applicable exception or limitation to copyright–then
 that use is not regulated by the license. Our licenses grant only permissions
 under copyright and certain other rights that a licensor has authority to
 grant. Use of the licensed material may still be restricted for other reasons,
 including because others have copyright or other rights in the material. A
 licensor may make special requests, such as asking that all changes be marked
 or described.
 .
 Although not required by our licenses, you are encouraged to respect those
 requests where reasonable. More considerations for the public : wiki.creativecommons.org/Considerations_for_licensees
 .
 Creative Commons Attribution-ShareAlike 4.0 International Public License
 .
 By exercising the Licensed Rights (defined below), You accept and agree to
 be bound by the terms and conditions of this Creative Commons Attribution-ShareAlike
 4.0 International Public License ("Public License"). To the extent this Public
 License may be interpreted as a contract, You are granted the Licensed Rights
 in consideration of Your acceptance of these terms and conditions, and the
 Licensor grants You such rights in consideration of benefits the Licensor
 receives from making the Licensed Material available under these terms and
 conditions.
 .
 Section 1 – Definitions.
 .
 a. Adapted Material means material subject to Copyright and Similar Rights
 that is derived from or based upon the Licensed Material and in which the
 Licensed Material is translated, altered, arranged, transformed, or otherwise
 modified in a manner requiring permission under the Copyright and Similar
 Rights held by the Licensor. For purposes of this Public License, where the
 Licensed Material is a musical work, performance, or sound recording, Adapted
 Material is always produced where the Licensed Material is synched in timed
 relation with a moving image.
 .
 b. Adapter's License means the license You apply to Your Copyright and Similar
 Rights in Your contributions to Adapted Material in accordance with the terms
 and conditions of this Public License.
 .
 c. BY-SA Compatible License means a license listed at creativecommons.org/compatiblelicenses,
 approved by Creative Commons as essentially the equivalent of this Public
 License.
 .
 d. Copyright and Similar Rights means copyright and/or similar rights closely
 related to copyright including, without limitation, performance, broadcast,
 sound recording, and Sui Generis Database Rights, without regard to how the
 rights are labeled or categorized. For purposes of this Public License, the
 rights specified in Section 2(b)(1)-(2) are not Copyright and Similar Rights.
 .
 e. Effective Technological Measures means those measures that, in the absence
 of proper authority, may not be circumvented under laws fulfilling obligations
 under Article 11 of the WIPO Copyright Treaty adopted on December 20, 1996,
 and/or similar international agreements.
 .
 f. Exceptions and Limitations means fair use, fair dealing, and/or any other
 exception or limitation to Copyright and Similar Rights that applies to Your
 use of the Licensed Material.
 .
 g. License Elements means the license attributes listed in the name of a Creative
 Commons Public License. The License Elements of this Public License are Attribution
 and ShareAlike.
 .
 h. Licensed Material means the artistic or literary work, database, or other
 material to which the Licensor applied this Public License.
 .
 i. Licensed Rights means the rights granted to You subject to the terms and
 conditions of this Public License, which are limited to all Copyright and
 Similar Rights that apply to Your use of the Licensed Material and that the
 Licensor has authority to license.
 .
 j. Licensor means the individual(s) or entity(ies) granting rights under this
 Public License.
 .
 k. Share means to provide material to the public by any means or process that
 requires permission under the Licensed Rights, such as reproduction, public
 display, public performance, distribution, dissemination, communication, or
 importation, and to make material available to the public including in ways
 that members of the public may access the material from a place and at a time
 individually chosen by them.
 .
 l. Sui Generis Database Rights means rights other than copyright resulting
 from Directive 96/9/EC of the European Parliament and of the Council of 11
 March 1996 on the legal protection of databases, as amended and/or succeeded,
 as well as other essentially equivalent rights anywhere in the world.
 .
 m. You means the individual or entity exercising the Licensed Rights under
 this Public License. Your has a corresponding meaning.
 .
 Section 2 – Scope.
 .
    a. License grant.
 .
 1. Subject to the terms and conditions of this Public License, the Licensor
 hereby grants You a worldwide, royalty-free, non-sublicensable, non-exclusive,
 irrevocable license to exercise the Licensed Rights in the Licensed Material
 to:
 .
          A. reproduce and Share the Licensed Material, in whole or in part; and
 .
          B. produce, reproduce, and Share Adapted Material.
 .
 2. Exceptions and Limitations. For the avoidance of doubt, where Exceptions
 and Limitations apply to Your use, this Public License does not apply, and
 You do not need to comply with its terms and conditions.
 .
       3. Term. The term of this Public License is specified in Section 6(a).
 .
 4. Media and formats; technical modifications allowed. The Licensor authorizes
 You to exercise the Licensed Rights in all media and formats whether now known
 or hereafter created, and to make technical modifications necessary to do
 so. The Licensor waives and/or agrees not to assert any right or authority
 to forbid You from making technical modifications necessary to exercise the
 Licensed Rights, including technical modifications necessary to circumvent
 Effective Technological Measures. For purposes of this Public License, simply
 making modifications authorized by this Section 2(a)(4) never produces Adapted
 Material.
 .
       5. Downstream recipients.
 .
 A. Offer from the Licensor – Licensed Material. Every recipient of the Licensed
 Material automatically receives an offer from the Licensor to exercise the
 Licensed Rights under the terms and conditions of this Public License.
 .
 B. Additional offer from the Licensor – Adapted Material. Every recipient
 of Adapted Material from You automatically receives an offer from the Licensor
 to exercise the Licensed Rights in the Adapted Material under the conditions
 of the Adapter's License You apply.
 .
 C. No downstream restrictions. You may not offer or impose any additional
 or different terms or conditions on, or apply any Effective Technological
 Measures to, the Licensed Material if doing so restricts exercise of the Licensed
 Rights by any recipient of the Licensed Material.
 .
 6. No endorsement. Nothing in this Public License constitutes or may be construed
 as permission to assert or imply that You are, or that Your use of the Licensed
 Material is, connected with, or sponsored, endorsed, or granted official status
 by, the Licensor or others designated to receive attribution as provided in
 Section 3(a)(1)(A)(i).
 .
    b. Other rights.
 .
 1. Moral rights, such as the right of integrity, are not licensed under this
 Public License, nor are publicity, privacy, and/or other similar personality
 rights; however, to the extent possible, the Licensor waives and/or agrees
 not to assert any such rights held by the Licensor to the limited extent necessary
 to allow You to exercise the Licensed Rights, but not otherwise.
 .
 2. Patent and trademark rights are not licensed under this Public License.
 .
 3. To the extent possible, the Licensor waives any right to collect royalties
 from You for the exercise of the Licensed Rights, whether directly or through
 a collecting society under any voluntary or waivable statutory or compulsory
 licensing scheme. In all other cases the Licensor expressly reserves any right
 to collect such royalties.
 .
 Section 3 – License Conditions.
 .
 Your exercise of the Licensed Rights is expressly made subject to the following
 conditions.
 .
    a. Attribution.
 .
 1. If You Share the Licensed Material (including in modified form), You must:
 .
 A. retain the following if it is supplied by the Licensor with the Licensed
 Material:
 .
 i. identification of the creator(s) of the Licensed Material and any others
 designated to receive attribution, in any reasonable manner requested by the
 Licensor (including by pseudonym if designated);
 .
             ii. a copyright notice;
 .
             iii. a notice that refers to this Public License;
 .
             iv. a notice that refers to the disclaimer of warranties;
 .
 v. a URI or hyperlink to the Licensed Material to the extent reasonably practicable;
 .
 B. indicate if You modified the Licensed Material and retain an indication
 of any previous modifications; and
 .
 C. indicate the Licensed Material is licensed under this Public License, and
 include the text of, or the URI or hyperlink to, this Public License.
 .
 2. You may satisfy the conditions in Section 3(a)(1) in any reasonable manner
 based on the medium, means, and context in which You Share the Licensed Material.
 For example, it may be reasonable to satisfy the conditions by providing a
 URI or hyperlink to a resource that includes the required information.
 .
 3. If requested by the Licensor, You must remove any of the information required
 by Section 3(a)(1)(A) to the extent reasonably practicable.
 .
 b. ShareAlike.In addition to the conditions in Section 3(a), if You Share
 Adapted Material You produce, the following conditions also apply.
 .
 1. The Adapter's License You apply must be a Creative Commons license with
 the same License Elements, this version or later, or a BY-SA Compatible License.
 .
 2. You must include the text of, or the URI or hyperlink to, the Adapter's
 License You apply. You may satisfy this condition in any reasonable manner
 based on the medium, means, and context in which You Share Adapted Material.
 .
 3. You may not offer or impose any additional or different terms or conditions
 on, or apply any Effective Technological Measures to, Adapted Material that
 restrict exercise of the rights granted under the Adapter's License You apply.
 .
 Section 4 – Sui Generis Database Rights.
 .
 Where the Licensed Rights include Sui Generis Database Rights that apply to
 Your use of the Licensed Material:
 .
 a. for the avoidance of doubt, Section 2(a)(1) grants You the right to extract,
 reuse, reproduce, and Share all or a substantial portion of the contents of
 the database;
 .
 b. if You include all or a substantial portion of the database contents in
 a database in which You have Sui Generis Database Rights, then the database
 in which You have Sui Generis Database Rights (but not its individual contents)
 is Adapted Material, including for purposes of Section 3(b); and
 .
 c. You must comply with the conditions in Section 3(a) if You Share all or
 a substantial portion of the contents of the database.
 .
 For the avoidance of doubt, this Section 4 supplements and does not replace
 Your obligations under this Public License where the Licensed Rights include
 other Copyright and Similar Rights.
 .
 Section 5 – Disclaimer of Warranties and Limitation of Liability.
 .
 a. Unless otherwise separately undertaken by the Licensor, to the extent possible,
 the Licensor offers the Licensed Material as-is and as-available, and makes
 no representations or warranties of any kind concerning the Licensed Material,
 whether express, implied, statutory, or other. This includes, without limitation,
 warranties of title, merchantability, fitness for a particular purpose, non-infringement,
 absence of latent or other defects, accuracy, or the presence or absence of
 errors, whether or not known or discoverable. Where disclaimers of warranties
 are not allowed in full or in part, this disclaimer may not apply to You.
 .
 b. To the extent possible, in no event will the Licensor be liable to You
 on any legal theory (including, without limitation, negligence) or otherwise
 for any direct, special, indirect, incidental, consequential, punitive, exemplary,
 or other losses, costs, expenses, or damages arising out of this Public License
 or use of the Licensed Material, even if the Licensor has been advised of
 the possibility of such losses, costs, expenses, or damages. Where a limitation
 of liability is not allowed in full or in part, this limitation may not apply
 to You.
 .
 c. The disclaimer of warranties and limitation of liability provided above
 shall be interpreted in a manner that, to the extent possible, most closely
 approximates an absolute disclaimer and waiver of all liability.
 .
 Section 6 – Term and Termination.
 .
 a. This Public License applies for the term of the Copyright and Similar Rights
 licensed here. However, if You fail to comply with this Public License, then
 Your rights under this Public License terminate automatically.
 .
 b. Where Your right to use the Licensed Material has terminated under Section
 6(a), it reinstates:
 .
 1. automatically as of the date the violation is cured, provided it is cured
 within 30 days of Your discovery of the violation; or
 .
       2. upon express reinstatement by the Licensor.
 .
 c. For the avoidance of doubt, this Section 6(b) does not affect any right
 the Licensor may have to seek remedies for Your violations of this Public
 License.
 .
 d. For the avoidance of doubt, the Licensor may also offer the Licensed Material
 under separate terms or conditions or stop distributing the Licensed Material
 at any time; however, doing so will not terminate this Public License.
 .
    e. Sections 1, 5, 6, 7, and 8 survive termination of this Public License.
 .
 Section 7 – Other Terms and Conditions.
 .
 a. The Licensor shall not be bound by any additional or different terms or
 conditions communicated by You unless expressly agreed.
 .
 b. Any arrangements, understandings, or agreements regarding the Licensed
 Material not stated herein are separate from and independent of the terms
 and conditions of this Public License.
 .
 Section 8 – Interpretation.
 .
 a. For the avoidance of doubt, this Public License does not, and shall not
 be interpreted to, reduce, limit, restrict, or impose conditions on any use
 of the Licensed Material that could lawfully be made without permission under
 this Public License.
 .
 b. To the extent possible, if any provision of this Public License is deemed
 unenforceable, it shall be automatically reformed to the minimum extent necessary
 to make it enforceable. If the provision cannot be reformed, it shall be severed
 from this Public License without affecting the enforceability of the remaining
 terms and conditions.
 .
 c. No term or condition of this Public License will be waived and no failure
 to comply consented to unless expressly agreed to by the Licensor.
 .
 d. Nothing in this Public License constitutes or may be interpreted as a limitation
 upon, or waiver of, any privileges and immunities that apply to the Licensor
 or You, including from the legal processes of any jurisdiction or authority.
 .
 Creative Commons is not a party to its public licenses. Notwithstanding, Creative
 Commons may elect to apply one of its public licenses to material it publishes
 and in those instances will be considered the "Licensor." The text of the
 Creative Commons public licenses is dedicated to the public domain under the
 CC0 Public Domain Dedication. Except for the limited purpose of indicating
 that material is shared under a Creative Commons public license or as otherwise
 permitted by the Creative Commons policies published at creativecommons.org/policies,
 Creative Commons does not authorize the use of the trademark "Creative Commons"
 or any other trademark or logo of Creative Commons without its prior written
 consent including, without limitation, in connection with any unauthorized
 modifications to any of its public licenses or any other arrangements, understandings,
 or agreements concerning use of licensed material. For the avoidance of doubt,
 this paragraph does not form part of the public licenses.
 .
 Creative Commons may be contacted at creativecommons.org.

License: BSD-2-Clause
 Copyright (c) <year> <owner>. All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
